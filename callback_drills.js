
/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
*/


let fs = require('fs')
let path = require('path');


function retriveData(idArray, file) {
    return fs.readFile(path.join(__dirname, file), (err, data) => {
        if (err) {
            err = new Error('error reading the file')
            console.log(err);
        } else {
            let infoArray = JSON.parse(data).employees.filter((info) => {
                if (idArray.includes(info.id)) {
                    return info
                }
            })
            fs.writeFile(path.join(__dirname, 'outputData1.json'), JSON.stringify(infoArray), (err, data) => {
                if (err) {
                    err = new Error('error in writing files')
                } else {
                    console.log('files written');

                }
            })
        }
    })
}

/*2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}*/

function groupData(data) {
    fs.readFile(path.join(__dirname, data), (err, data) => {
        if (err) {
            err = new Error('error writing files')
            console.log(err);
        } else {
            let companyGroup = JSON.parse(data).employees.reduce((accu, info) => {
                if (accu.hasOwnProperty(info.company)) {
                    accu[info.company].push(info)
                } else {
                    accu[info.company] = []
                    accu[info.company].push(info)
                }
                return accu
            }, {})

            fs.writeFile(path.join(__dirname, 'outputData2.json'), JSON.stringify(companyGroup), (err, data) => {
                if (err) {
                    err = new Error('error writing files')
                } else {
                    console.log('files written');
                }
            })
        }
    })
}
// 3. Get all data for company Powerpuff Brigade

function retriveCompanyData(companyName, file) {
    return fs.readFile(path.join(__dirname, file), (err, data) => {
        if (err) {
            err = new Error('error reading the file')
            console.log(err);
        } else {
            let infoArray = JSON.parse(data).employees.filter((info) => {
                if (info.company==companyName) {
                    return info
                }
            })
            fs.writeFile(path.join(__dirname, 'outputData3.json'), JSON.stringify(infoArray), (err, data) => {
                if (err) {
                    err = new Error('error in writing files')
                } else {
                    console.log('files written');

                }
            })
        }
    })
}


// 4. Remove entry with id 2.

function deleteData(id, file) {
    return fs.readFile(path.join(__dirname, file), (err, data) => {
        if (err) {
            err = new Error('error reading the file')
            console.log(err);
        } else {
            let infoArray = JSON.parse(data).employees.filter((info) => {
                if (info.id !== id) {
                    return info
                }
            })
            fs.writeFile(path.join(__dirname, 'outputData4.json'), JSON.stringify(infoArray), (err, data) => {
                if (err) {
                    err = new Error('error in writing files')
                } else {
                    console.log('files written');

                }
            })
        }
    })
}

// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function sortCompany(file)
{
    return fs.readFile(path.join(__dirname, file), (err, data) => {
        if (err) {
            err = new Error('error reading the file')
            console.log(err);
        } else {
            let infoArray = JSON.parse(data).employees.sort((first, second) => {
               if(first.company == second.company)
               {
                return first.id - second.id
               } else {
                return first['company'].localeCompare(second['company'])
               }
            })
            fs.writeFile(path.join(__dirname, 'outputData5.json'), JSON.stringify(infoArray), (err, data) => {
                if (err) {
                    err = new Error('error in writing files')
                } else {
                    console.log('files written');

                }
            })
        }
    })
}
sortCompany('data.json')
